console.log('Задание 1');

var studentsAndPoints = [
    'Алексей Петров', 0,
    'Ирина Овчинникова', 60,
    'Глеб Стукалов', 30,
    'Антон Павлович', 30,
    'Виктория Заровская', 30,
    'Алексей Левенец', 70,
    'Тимур Вамуш', 30,
    'Евгений Прочан', 60,
    'Александр Малов', 0
];

var students = [];
var student;
var commonShow = function () {
    console.log('Студент %s набрал %d баллов', this.name, this.point);
};

for (var i = 0; i < studentsAndPoints.length - 1; i += 2) {
    student = {};
    student.name = studentsAndPoints[i];
    student.point = studentsAndPoints[i + 1];
    student.show = commonShow;
    students.push(student);
}
students.forEach(function(item, i) {
    item.show();
});

console.log('Задание 2');

student = {
    name: 'Николай Фролов',
    point: 0,
    show: commonShow
};

students.push(student);

students.push({
    name: 'Олег Боровой',
    point: 0,
    show: commonShow
});

students.forEach(function(item) {
    item.show();
});

console.log('Задание 3');
function addPoint(name, point) {
    var student = students.find(function(item) {
        return name == item.name;
    });
    if (typeof student != 'undefined') {
        student.point += point;
        return student;
    } else {
        console.log('Студент %s не найден', name);
        return false;
    }
}

addPoint('Ирина Овчинникова', 30);
addPoint('Александр Малов', 30);
addPoint('Николай Фролов', 10);
addPoint('Дмитрий Фитискин', 10);

students.forEach(function(item) {
    item.show();
});

console.log('Задание 4');

students.forEach(function(item) {
    if (item.point >= 30) {
        item.show();
    }
});

console.log('Задание 5');

students.forEach(function(item) {
    item.worksAmount = item.point / 10;
});

console.log(students);

console.log('Дополнительное задание');

students.findByName = function(name) {
    return students.find(function(item) {
        return name == item.name;
    });
};

console.log(students.findByName('Ирина Овчинникова'));

console.log(students.findByName('Иван Иванов'));














